import java.util.concurrent.RecursiveTask;

public class RecSumOfN extends RecursiveTask<Long> {
    private static long N = 20L;

    long from;
    long to;

    public RecSumOfN(long from, long to) {
        this.from = from;
        this.to = to;
    }

    @Override
    protected Long compute() {
        if ((to - from) < N) {
            long localSum = 0L;
            for (long i = from; i <= to; i++) {
                localSum += i;
            }
            return localSum;
        } else {
            long mid = (from + to) / 2;
            RecSumOfN firstHalf = new RecSumOfN(from, mid);
            firstHalf.fork();
            RecSumOfN secondHalf = new RecSumOfN(mid + 1, to);
            long resultSecond = secondHalf.compute();
            return firstHalf.join() + resultSecond;
        }
    }
}

