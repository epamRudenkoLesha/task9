import java.util.concurrent.ForkJoinPool;

public class Main {

    private static final int NUM_THREADS = 8;
    private static final long N = 1_000_000L;

    public static void main(String[] args) {
        ForkJoinPool pool = new ForkJoinPool(NUM_THREADS);
        long computeSum = pool.invoke(new RecSumOfN(0, N));
        long formulaSum = (N * (N +1))/2;
        System.out.printf("Sum for range 1..%d:\n" +
                "computed sum = %d\n" +
                "formula sum = %d %n", N, computeSum, formulaSum);
    }
}
