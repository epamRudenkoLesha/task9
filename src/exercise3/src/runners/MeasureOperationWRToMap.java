package runners;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicLong;

public abstract class MeasureOperationWRToMap implements Runnable {

    private AtomicLong time = new AtomicLong(0L);
    protected MapsRunner mapsRunner;
    protected CountDownLatch cdl;

    public MeasureOperationWRToMap(MapsRunner mapsRunner) {
        this.mapsRunner = mapsRunner;
    }

    public double getTime() {
        return time.doubleValue() / 1_000_000_000.0;
    }

    public void addTime(long time) {
        this.time = new AtomicLong(this.time.getAndAdd(time));
    }

    public void setCdl(CountDownLatch cdl) {
        this.cdl = cdl;
    }
}
