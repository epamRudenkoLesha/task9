package runners.readers;

import runners.MapsRunner;
import runners.MeasureOperationWRToMap;

public class ReadConcMap extends MeasureOperationWRToMap {

    public ReadConcMap(MapsRunner mapsRunner) {
        super(mapsRunner);
    }

    public void run() {
        int size = mapsRunner.getSize();
        for (int i = 0; i < size; i++) {
            String key = "" + i;
            new ReadFromConcMap(key, cdl, mapsRunner, this);
        }
    }


}
