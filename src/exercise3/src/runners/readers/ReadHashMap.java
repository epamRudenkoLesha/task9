package runners.readers;

import runners.MapsRunner;
import runners.MeasureOperationWRToMap;

public class ReadHashMap extends MeasureOperationWRToMap {

    public ReadHashMap(MapsRunner mapsRunner) {
        super(mapsRunner);
    }

    public void run() {
        int size = mapsRunner.getSize();
        for (int i = 0; i < size; i++) {
            String key = "" + i;
            new ReadFromHashMap(key, cdl, mapsRunner, this);
        }
    }


}
