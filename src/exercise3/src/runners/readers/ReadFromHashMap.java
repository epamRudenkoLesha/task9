package runners.readers;

import runners.MapsRunner;

import java.util.concurrent.CountDownLatch;

public class ReadFromHashMap implements Runnable {

    private String key;
    private MapsRunner runner;
    private final ReadHashMap readHashMap;
    private CountDownLatch cdl;

    public ReadFromHashMap(String key, CountDownLatch cdl, MapsRunner mapsRunner, ReadHashMap readHashMap) {
        this.key = key;
        this.cdl = cdl;
        runner = mapsRunner;
        this.readHashMap = readHashMap;
        new Thread(this).start();
    }


    @Override
    public void run() {
        long v = runner.measureHashMapRead(key);
        readHashMap.addTime(v);
        cdl.countDown();
    }
}
