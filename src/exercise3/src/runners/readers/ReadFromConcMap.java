package runners.readers;

import runners.MapsRunner;

import java.util.concurrent.CountDownLatch;

public class ReadFromConcMap implements Runnable {

    private String key;
    private MapsRunner runner;
    private ReadConcMap readConcMap;
    private CountDownLatch cdl;

    public ReadFromConcMap(String key, CountDownLatch cdl, MapsRunner runner, ReadConcMap readConcMap) {
        this.key = key;
        this.cdl = cdl;
        this.runner = runner;
        this.readConcMap = readConcMap;
        new Thread(this).start();
    }

    @Override
    public void run() {
        long v = runner.measureConcMapRead(key);
        readConcMap.addTime(v);
        cdl.countDown();
    }
}
