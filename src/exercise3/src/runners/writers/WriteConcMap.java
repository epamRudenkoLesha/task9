package runners.writers;

import runners.MapsRunner;
import runners.MeasureOperationWRToMap;

import java.util.List;

public class WriteConcMap extends MeasureOperationWRToMap {

    public WriteConcMap(MapsRunner mapsRunner) {
        super(mapsRunner);
    }

    public void run() {
        int size = mapsRunner.getSize();
        List<Integer> integers = mapsRunner.getIntegers();
        for (int i = 0; i < size; i++) {
            new WriteToConcMap(integers.get(i), cdl, mapsRunner, this);
        }
    }


}
