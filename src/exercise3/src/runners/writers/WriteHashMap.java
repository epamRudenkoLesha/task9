package runners.writers;

import runners.MapsRunner;
import runners.MeasureOperationWRToMap;

import java.util.List;

public class WriteHashMap extends MeasureOperationWRToMap {

    public WriteHashMap(MapsRunner mapsRunner) {
        super(mapsRunner);
    }

    public void run() {
        int size = mapsRunner.getSize();
        List<Integer> integers = mapsRunner.getIntegers();
        for (int i = 0; i < size; i++) {
            new WriteToHashMap(integers.get(i), cdl, mapsRunner, this);
        }
    }


}
