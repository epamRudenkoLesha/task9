package runners.writers;

import runners.MapsRunner;

import java.util.concurrent.CountDownLatch;

public class WriteToConcMap implements Runnable {

    private int value;
    private MapsRunner runner;
    private WriteConcMap writeConcMap;
    private CountDownLatch cdl;

    public WriteToConcMap(int value, CountDownLatch cdl, MapsRunner runner, WriteConcMap writeConcMap) {
        this.value = value;
        this.cdl = cdl;
        this.runner = runner;
        this.writeConcMap = writeConcMap;
        new Thread(this).start();
    }

    @Override
    public void run() {
        long v = runner.measureConcMapWrite(value);
        writeConcMap.addTime(v);
        cdl.countDown();
    }
}
