package runners.writers;

import runners.MapsRunner;

import java.util.concurrent.CountDownLatch;

public class WriteToHashMap implements Runnable {

    private int value;
    private MapsRunner runner;
    private WriteHashMap writeHashMap;
    private CountDownLatch cdl;

    public WriteToHashMap(int value, CountDownLatch cdl, MapsRunner runner, WriteHashMap writeHashMap) {
        this.value = value;
        this.cdl = cdl;
        this.runner = runner;
        this.writeHashMap = writeHashMap;
        new Thread(this).start();
    }

    @Override
    public void run() {
        long v = runner.measureHashMapWrite(value);
        writeHashMap.addTime(v);
        cdl.countDown();
    }
}
