package runners;

import runners.readers.ReadHashMap;
import runners.writers.WriteConcMap;
import runners.writers.WriteHashMap;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CountDownLatch;

public class MapsRunner {

    private int size = 1_000_000;
    private List<Integer> integers;
    private Map<String, Integer> hashMap;
    private Map<String, Integer> concMap;

    public MapsRunner() {
    }

    public void run() {
        integers = generateIntList(size);
        hashMap = new HashMap<>(size);
        concMap = new ConcurrentHashMap<>(size);
        double timeElapsed;
        try {
            timeElapsed = getTimeElapsed(new WriteHashMap(this));
            System.out.printf("Writing to hashMap: %7.12f sec\n", timeElapsed);
            timeElapsed = getTimeElapsed(new WriteConcMap(this));
            System.out.printf("Writing to concMap: %7.12f sec\n", timeElapsed);
            timeElapsed = getTimeElapsed(new ReadHashMap(this));
            System.out.printf("Reading from hashMap: %7.12f sec\n", timeElapsed);
            timeElapsed = getTimeElapsed(new WriteConcMap(this));
            System.out.printf("Reading from concMap: %7.12f sec\n", timeElapsed);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private double getTimeElapsed(MeasureOperationWRToMap measurer) throws InterruptedException {
        double timeElapsed;
        CountDownLatch cdl = new CountDownLatch(size);
        measurer.setCdl(cdl);
        new Thread(measurer).start();
        cdl.await();
        timeElapsed = measurer.getTime();
        return timeElapsed;
    }

    public long measureHashMapWrite(int index) {
        long timeS = 0;
        long timeF = 0;
        String key = "" + index;
        Integer value = integers.get(index);
        timeS = System.nanoTime();
        synchronized (hashMap) {
            hashMap.put(key, value);
        }
        timeF = System.nanoTime();
        return timeF - timeS;
    }

    public long measureConcMapWrite(int index) {
        long timeS = 0;
        long timeF = 0;
        String key = "" + index;
        Integer value = integers.get(index);
        timeS = System.nanoTime();
        concMap.put(key, value);
        timeF = System.nanoTime();
        return timeF - timeS;
    }

    public long measureHashMapRead(String key) {
        long timeS = 0;
        long timeF = 0;
        timeS = System.nanoTime();
        synchronized (hashMap) {
            hashMap.get(key);
        }
        timeF = System.nanoTime();
        return timeF - timeS;
    }

    public long measureConcMapRead(String key) {
        long timeS = 0;
        long timeF = 0;
        timeS = System.nanoTime();
        concMap.get(key);
        timeF = System.nanoTime();
        return timeF - timeS;
    }

    private List<Integer> generateIntList(int size) {
        List<Integer> integers = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            integers.add((int) (Math.random() * size));
        }
        return integers;
    }

    public int getSize() {
        return size;
    }

    public List<Integer> getIntegers() {
        return integers;
    }
}
