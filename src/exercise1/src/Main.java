import java.util.stream.Stream;

public class Main {

    public static void main(String[] args) {
        Thread simpleThread = new Thread(() -> Stream
                .iterate(10, i -> i - 1)
                .limit(11)
                .forEach(integer -> {
                    try {
                        Thread.sleep(1000);
                        if (integer == 0) {
                            System.out.println("Boom!");
                        } else {
                            System.out.println(integer);
                        }
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }));
        simpleThread.start();
    }
}
