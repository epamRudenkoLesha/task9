import javafx.util.Pair;

import java.io.*;
import java.util.Scanner;
import java.util.concurrent.SynchronousQueue;

public class Main {
    private static final String RESULT_TXT = "result.txt";

    public static void main(String[] args) throws InterruptedException {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter directory -> ");
        String dir = sc.next();
        System.out.print("Enter symbol -> ");
        char symbol = sc.next().toCharArray()[0];
        SynchronousQueue<Pair<String, Integer>> drop = new SynchronousQueue<>();
        FileWriterResult writer = new FileWriterResult(drop, RESULT_TXT);
        Thread fileWriterThread = new Thread(writer);
        fileWriterThread.start();
        FileScanner counter = new FileScanner(drop, new File(dir), symbol);
        Thread fileScannerThread = new Thread(counter);
        fileScannerThread.start();
        fileScannerThread.join();
        drop.put(new Pair<>("DONE", 0));
        fileWriterThread.join();

        try (BufferedReader br = new BufferedReader(new FileReader(RESULT_TXT))) {
            String str;
            while ((str = br.readLine()) != null) {
                System.out.println(str);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }


//        try {
//            System.out.println(task.get() + " files.");
//        } catch (ExecutionExc eption | InterruptedException e) {
//            e.printStackTrace();
//        }
    }
}
