import javafx.util.Pair;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Scanner;
import java.util.concurrent.SynchronousQueue;

public class FileScanner implements Runnable {
    private SynchronousQueue<Pair<String, Integer>> drop;
    private File dir;
    private char symbol;

    public FileScanner(SynchronousQueue<Pair<String, Integer>> drop, File dir, char symbol) {
        this.drop = drop;
        this.dir = dir;
        this.symbol = symbol;
    }

    public int search(File file) {
        int counter = 0;
        try (Scanner sc = new Scanner(new FileInputStream(file))) {
            while (sc.hasNextLine()) {
                String str = sc.nextLine();
                String[] split = str.split("\\b");
                for (String s : split) {
                    if (!s.isEmpty() && s.toCharArray()[0] == symbol) {
                        counter++;
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return counter;
    }

    @Override
    public void run() {
        int wordsCounter;
        File[] files = dir.listFiles();
        for (File ff : files) {
            if (ff.isDirectory()) {
                FileScanner counter = new FileScanner(drop, ff, symbol);
                Thread thread = new Thread(counter);
                thread.start();
                try {
                    thread.join();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            } else if ((wordsCounter = search(ff)) > 0) {
                try {
                    drop.put(new Pair<>(ff.getName(), wordsCounter));
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
