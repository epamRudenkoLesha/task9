import javafx.util.Pair;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.concurrent.SynchronousQueue;

public class FileWriterResult implements Runnable {

    private SynchronousQueue<Pair<String, Integer>> drop;
    private String fileName;

    public FileWriterResult(SynchronousQueue<Pair<String, Integer>> drop, String fileName) {
        this.drop = drop;
        this.fileName = fileName;
    }

    @Override
    public void run() {
        Pair<String, Integer> msg = null;
        try (BufferedWriter bw = new BufferedWriter(
                new FileWriter(new File(fileName)))) {
            while (!(msg = drop.take()).getKey().equals("DONE")) {
                bw.write(msg.getKey());
                bw.write(" contains ");
                bw.write(msg.getValue().toString());
                bw.write(" words.\n");
                bw.flush();
            }
        } catch (InterruptedException | IOException e) {
            e.printStackTrace();
        }
    }
}
