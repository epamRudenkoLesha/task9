public class Printer implements Runnable {
    CounterValue counterValue;

    public Printer(CounterValue counterValue) {
        this.counterValue = counterValue;
        new Thread(this).start();
    }

    @Override
    public void run() {
        while (true) {
            if (counterValue.getCounter() < 1_000) {
                counterValue.printCounter();
            } else {
                break;
            }
        }
    }
}
