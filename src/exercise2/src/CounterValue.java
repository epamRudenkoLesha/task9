public class CounterValue {

    int counter = 0;
    boolean changed = false;

    synchronized void increment() {
        while (changed) {
            try {
                wait();
            } catch (InterruptedException e) {
                System.out.println(e);
            }
        }
        counter++;
        changed = true;
        notify();
    }

    synchronized void printCounter() {
        while (!changed) {
            try {
                wait();
            } catch (InterruptedException e) {
                System.out.println(e);
            }
        }
        changed = false;
        System.out.println(counter);
        notify();
    }

    public int getCounter() {
        return counter;
    }
}
