public class Counter implements Runnable {
    CounterValue counterValue;

    public Counter(CounterValue counterValue) {
        this.counterValue = counterValue;
        new Thread(this).start();
    }

    @Override
    public void run() {
        while (true) {
            if (counterValue.getCounter() < 1_000) {
                counterValue.increment();
            } else {
                break;
            }
        }
    }
}
